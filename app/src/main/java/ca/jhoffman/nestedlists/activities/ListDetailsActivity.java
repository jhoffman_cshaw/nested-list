package ca.jhoffman.nestedlists.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import ca.jhoffman.nestedlists.R;
import ca.jhoffman.nestedlists.activities.adapters.ListItemAdapter;
import ca.jhoffman.nestedlists.activities.adapters.ListsAdapter;
import ca.jhoffman.nestedlists.model.ListContainer;
import ca.jhoffman.nestedlists.model.ListsProvider;

public class ListDetailsActivity extends AppCompatActivity {
    public static final String KEY_POSITION = "ca.jhoffman.nestedlists.KEY_POSITION";

    private TextView listNameTextView;
    private TextView itemsCountTextView;
    private EditText itemNameEditText;
    private ListItemAdapter itemsAdapter;

    private ListContainer currentListContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_details);

        int listPosition = getIntent().getExtras().getInt(KEY_POSITION);
        currentListContainer = ListsProvider.getInstance().getItemAtPosition(listPosition);

        listNameTextView = (TextView)findViewById(R.id.activity_list_details_list_name_textview);
        listNameTextView.setText(currentListContainer.getName());

        itemsCountTextView = (TextView)findViewById(R.id.activity_list_details_list_items_count_textview);
        itemsCountTextView.setText(currentListContainer.getItemsCount() +" items");

        itemNameEditText = (EditText)findViewById(R.id.activity_list_details_new_item_name_edittext);
        findViewById(R.id.activity_list_details_add_item_button).setOnClickListener(addItemButtonClickListener);

        ListView listview = (ListView)findViewById(R.id.activity_list_details_listview);
        itemsAdapter = new ListItemAdapter(this, currentListContainer);
        listview.setAdapter(itemsAdapter);

        listview.setOnItemLongClickListener(onItemsListLongClickListener);
    }


    private View.OnClickListener addItemButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String itemName = itemNameEditText.getText().toString();

            if (itemName.isEmpty() == false) {
                itemNameEditText.setText("");

                currentListContainer.addItem(itemName);
                dataChanged();
            }
        }
    };

    private AdapterView.OnItemLongClickListener onItemsListLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            currentListContainer.deleteItemAtPosition(position);
            dataChanged();

            return true;
        }
    };

    private void dataChanged() {
        itemsCountTextView.setText(currentListContainer.getItemsCount() +" items");
        itemsAdapter.notifyDataSetChanged();
    }
}
