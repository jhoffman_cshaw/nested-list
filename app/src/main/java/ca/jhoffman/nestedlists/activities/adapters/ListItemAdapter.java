package ca.jhoffman.nestedlists.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ca.jhoffman.nestedlists.R;
import ca.jhoffman.nestedlists.model.ListContainer;
import ca.jhoffman.nestedlists.model.ListsProvider;

/**
 * Created by jhoffman on 2016-10-05.
 */

public class ListItemAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ListContainer listContainer;

    public ListItemAdapter(Context context, ListContainer listContainer) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.listContainer = listContainer;
    }


    @Override
    public int getCount() {
        return listContainer.getItemsCount();
    }

    @Override
    public Object getItem(int position) {
        return listContainer.getItemAtPosition(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.row_list_item, parent, false);

        TextView name = (TextView)view.findViewById(R.id.row_list_item_name);
        String itemName = (String)getItem(position);
        name.setText(position + "- " + itemName);

        return view;
    }
}

