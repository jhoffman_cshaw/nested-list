package ca.jhoffman.nestedlists.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.zip.Inflater;

import ca.jhoffman.nestedlists.R;
import ca.jhoffman.nestedlists.model.ListContainer;
import ca.jhoffman.nestedlists.model.ListsProvider;

/**
 * Created by jhoffman on 2016-10-05.
 */
public class ListsAdapter extends BaseAdapter{

    private LayoutInflater inflater;

    public ListsAdapter(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return ListsProvider.getInstance().getListsCount();
    }

    @Override
    public Object getItem(int position) {
        return ListsProvider.getInstance().getItemAtPosition(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.row_lists, parent, false);

        TextView name = (TextView)view.findViewById(R.id.row_lists_name);
        TextView itemsCounts = (TextView)view.findViewById(R.id.row_lists_items_count);

        ListContainer list = (ListContainer)getItem(position);
        name.setText(list.getName());
        itemsCounts.setText("Items: " + list.getItemsCount());

        return view;
    }
}
