package ca.jhoffman.nestedlists.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import ca.jhoffman.nestedlists.R;
import ca.jhoffman.nestedlists.activities.adapters.ListsAdapter;
import ca.jhoffman.nestedlists.model.ListContainer;
import ca.jhoffman.nestedlists.model.ListsProvider;

public class ListsActivity extends AppCompatActivity {

    private EditText listNameEditText;
    private ListsAdapter listsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists);

        listNameEditText = (EditText)findViewById(R.id.activity_lists_new_list_name_edittext);
        findViewById(R.id.activity_lists_add_list_button).setOnClickListener(addListButtonClickListener);

        ListView listview = (ListView)findViewById(R.id.activity_lists_listview);
        listsAdapter = new ListsAdapter(this);
        listview.setAdapter(listsAdapter);
        listview.setOnItemClickListener(onListItemClickedListener);
        listview.setOnItemLongClickListener(onListLongClickListener);
    }

    private View.OnClickListener addListButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String newItemName = listNameEditText.getText().toString();

            if (newItemName.isEmpty() == false) {
                listNameEditText.setText("");

                ListsProvider.getInstance().addList(new ListContainer(newItemName));
                listsAdapter.notifyDataSetChanged();
            }
        }
    };

    private AdapterView.OnItemClickListener onListItemClickedListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent i = new Intent(ListsActivity.this, ListDetailsActivity.class);
            i.putExtra(ListDetailsActivity.KEY_POSITION, position);
            startActivity(i);
        }
    };


    private AdapterView.OnItemLongClickListener onListLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            ListsProvider.getInstance().deleteListAtPosition(position);
            listsAdapter.notifyDataSetChanged();

            return true;
        }
    };
}
