package ca.jhoffman.nestedlists.model;

import java.util.ArrayList;

/**
 * Created by jhoffman on 2016-10-05.
 */

public class ListContainer {
    private String name;
    private ArrayList<String> items;

    public String getName() {
        return name;
    }

    public int getItemsCount() {
        return items.size();
    }

    public ListContainer() {
        this("");
    }

    public ListContainer(String name) {
        this.name = name;
        items = new ArrayList<>();
    }

    public void addItem(String item) {
        items.add(item);
    }

    public String getItemAtPosition(int position) {
        return items.get(position);
    }

    public void deleteItemAtPosition(int position) {
        items.remove(position);
    }
}
