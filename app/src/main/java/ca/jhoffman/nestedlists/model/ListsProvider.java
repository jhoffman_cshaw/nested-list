package ca.jhoffman.nestedlists.model;

import java.util.ArrayList;

/**
 * Created by jhoffman on 2016-10-05.
 */

public class ListsProvider {
    private static ListsProvider instance = new ListsProvider();

    private ArrayList<ListContainer> lists;

    public static ListsProvider getInstance() {
        return instance;
    }

    private ListsProvider() {
        lists = new ArrayList<>();

        // Dummy data
        ListContainer list1 = new ListContainer("Liste 1");
        list1.addItem("1 a");
        list1.addItem("1 b");
        list1.addItem("1 c");
        list1.addItem("1 d");
        list1.addItem("1 e");

        ListContainer list2 = new ListContainer("Liste 2");
        list2.addItem("2 aa");
        list2.addItem("2 bb");
        list2.addItem("2 cc");

        lists.add(list1);
        lists.add(list2);
    }

    public int getListsCount() {
        return lists.size();
    }

    public ListContainer getItemAtPosition(int position) {
        return lists.get(position);
    }

    public void addList(ListContainer listContainer) {
        lists.add(listContainer);
    }

    public void deleteListAtPosition(int position) {
        lists.remove(position);
    }
}
